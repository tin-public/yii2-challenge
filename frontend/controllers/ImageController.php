<?php
namespace frontend\controllers;

use common\models\Image;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class ImageController extends Controller
{
    const DISPLAY_COUNT = 12;

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $category_id = $request->get('category-id');
        // $sort_new = $request->get('new');
        $sort_new = true;
        $page = $request->get('page');
        $cache = true;

        $count = Image::countByParams(compact('category_id'));
        $pages = new Pagination(['totalCount' => $count, 'pageSize' => self::DISPLAY_COUNT]);
        $limit = $pages->limit;
        $offset = $page <= 1 || $page > $pages->pageCount ? null : self::DISPLAY_COUNT * $page;

        $images = Image::findByParams(compact('category_id', 'sort_new', 'limit', 'offset' , 'cache'));
        $title = 'My images (Using DbDependency cache)';

        return $this->renderPartial('index.twig', compact('images', 'pages', 'title'));
    }

    public function actionLazyLoad()
    {
        $request = Yii::$app->request;
        $page = $request->get('page');

        $count = Image::countByCaptionEnd1();
        $pages = new Pagination(['totalCount' => $count, 'pageSize' => self::DISPLAY_COUNT]);

        $images = Image::findCaptionEnd1();
        $title = 'My images (LazyLoad)';

        return $this->renderPartial('index.twig', compact('images', 'pages', 'title'));
    }

    public function actionEagerLoad()
    {
        $request = Yii::$app->request;
        $page = $request->get('page');

        $count = Image::countByCaptionEnd1();
        $pages = new Pagination(['totalCount' => $count, 'pageSize' => self::DISPLAY_COUNT]);

        $images = Image::findCaptionEnd1UsingEagerLoad();
        $title = 'My images (EagerLoad)';

        return $this->renderPartial('index.twig', compact('images', 'pages', 'title'));
    }
}
