<?php
namespace frontend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class DiContainerController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors(): array
    {
        return [
            [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $method = $request->method;
        if ($request->isGet) {
            $id = $request->get('id');
            $name = $request->get('name');
        } else {
            $id = $request->post('id');
            $name = $request->post('name');
        }

        $id = filter_var($id, FILTER_VALIDATE_INT, [
            'options' => [
                'min_rage' => 0,
                'default' => 0,
            ]
        ]);

        if (!preg_match('/^[\w\d ]+$/', $name)) {
            $name = '-';
        }

        $data = compact('method', 'id', 'name');
        Yii::info(json_encode($data));
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }
}
