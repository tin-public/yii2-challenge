<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class TopController extends Controller
{
    public function actionIndex()
    {
        $constant = STANDARD_DATETIME_FORMAT;
        $param_var = Yii::$app->params['param_var'];
        return $this->renderPartial('index.twig', compact('constant', 'param_var'));
    }
}
