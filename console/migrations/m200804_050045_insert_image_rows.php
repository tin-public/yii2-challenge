<?php

use yii\db\Migration;

/**
 * Class m200804_050045_insert_image_rows
 */
class m200804_050045_insert_image_rows extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = ['caption', 'image', 'category_id', 'created_at', 'updated_at'];
        $this->batchInsert('{{%image}}', $columns ,$this->makeData(1, 'Abstract', 'http://lorempixel.com/256/256/abstract', new DateTime('2020-01-01')));
        $this->batchInsert('{{%image}}', $columns ,$this->makeData(2, 'Animals', 'http://lorempixel.com/256/256/animals', new DateTime('2020-02-01')));
        $this->batchInsert('{{%image}}', $columns ,$this->makeData(3, 'Business', 'http://lorempixel.com/256/256/business', new DateTime('2020-03-01')));
        $this->batchInsert('{{%image}}', $columns ,$this->makeData(4, 'Cats', 'http://lorempixel.com/256/256/cats', new DateTime('2020-04-01')));
        $this->batchInsert('{{%image}}', $columns ,$this->makeData(5, 'City', 'http://lorempixel.com/256/256/city', new DateTime('2020-05-01')));
        $this->batchInsert('{{%image}}', $columns ,$this->makeData(6, 'Food', 'http://lorempixel.com/256/256/food', new DateTime('2020-06-01')));
        $this->batchInsert('{{%image}}', $columns ,$this->makeData(7, 'Nightlife', 'http://lorempixel.com/256/256/nightlife', new DateTime('2020-07-01')));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('{{%image}}');
    }

    private function makeData(int $category_id, string $category_name, string $link_prefix, DateTime $date, int $size = 10): array
    {
        $result = [];
        $created_date = clone $date;
        for ($i=1; $i <= $size; $i++) {
            $created_date_str = $created_date->format(STANDARD_DATETIME_FORMAT);
            $result[] = [
                'caption' => "$category_name $i",
                'image' => "$link_prefix/$i",
                'category_id' => $category_id,
                'created_at' => $created_date_str,
                'updated_at' => $created_date_str,
            ];
            $created_date->add(new DateInterval('P1D'));
        }
        return $result;
    }
}
