<?php

use yii\db\Migration;

/**
 * Class m200804_034714_insert_category_rows
 */
class m200804_034714_insert_category_rows extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data = [
            [
                'category_id' => 1,
                'name' => 'Abstract',
                'thumbnail' => 'http://lorempixel.com/256/256/abstract/1',
                'description' => 'Abstract images',
                'updated_at' => (new DateTime())->format(STANDARD_DATETIME_FORMAT),
            ],
            [
                'category_id' => 2,
                'name' => 'Animals',
                'thumbnail' => 'http://lorempixel.com/256/256/animals/1',
                'updated_at' => (new DateTime())->format(STANDARD_DATETIME_FORMAT),
            ],
            [
                'category_id' => 3,
                'name' => 'Business',
                'thumbnail' => 'http://lorempixel.com/256/256/business/1',
                'updated_at' => (new DateTime())->format(STANDARD_DATETIME_FORMAT),
            ],
            [
                'category_id' => 4,
                'name' => 'Cats',
                'thumbnail' => 'http://lorempixel.com/256/256/cats/1',
                'updated_at' => (new DateTime())->format(STANDARD_DATETIME_FORMAT),
            ],
            [
                'category_id' => 5,
                'name' => 'City',
                'thumbnail' => 'http://lorempixel.com/256/256/city/1',
                'updated_at' => (new DateTime())->format(STANDARD_DATETIME_FORMAT),
            ],
            [
                'category_id' => 6,
                'name' => 'Food',
                'thumbnail' => 'http://lorempixel.com/256/256/food/1',
                'updated_at' => (new DateTime())->format(STANDARD_DATETIME_FORMAT),
            ],
            [
                'category_id' => 7,
                'name' => 'Nightlife',
                'thumbnail' => 'http://lorempixel.com/256/256/nightlife/1',
                'updated_at' => (new DateTime())->format(STANDARD_DATETIME_FORMAT),
            ],
        ];
        foreach ($data as $row) {
            $this->insert('{{%category}}', $row);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i=1; $i < 8; $i++) { 
            $this->delete('{{%category}}', ['category_id' => $i]);
        }
    }
}
