<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%image}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%category}}`
 */
class m200804_034424_create_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%image}}', [
            'image_id' => $this->primaryKey(),
            'caption' => $this->string(100)->notNull(),
            'description' => $this->text(),
            'image' => $this->string(2083)->notNull(),
            'category_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->notNull()->append('ON UPDATE CURRENT_TIMESTAMP'),
            'deleted_at' => $this->dateTime(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-image-category_id}}',
            '{{%image}}',
            'category_id'
        );

        // add foreign key for table `{{%category}}`
        $this->addForeignKey(
            '{{%fk-image-category_id}}',
            '{{%image}}',
            'category_id',
            '{{%category}}',
            'category_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%category}}`
        $this->dropForeignKey(
            '{{%fk-image-category_id}}',
            '{{%image}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-image-category_id}}',
            '{{%image}}'
        );

        $this->dropTable('{{%image}}');
    }
}
