<?php

namespace common\log;

use yii\log\DbTarget;


class DbLogger extends DbTarget
{
    use MessagePrefixTrait;
}
