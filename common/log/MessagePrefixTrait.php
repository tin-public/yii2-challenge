<?php

namespace common\log;

use Yii;

trait MessagePrefixTrait {
    public function getMessagePrefix($message)
    {
        $server_ip = $_SERVER['SERVER_ADDR'] ?: '-';
        $remote_ip = $_SERVER['REMOTE_ADDR'] ?: '-';

        $request = Yii::$app->request;
        $method = $request->method;
        if ($request->isGet) {
            $id = $request->get('id');
            $name = $request->get('name');
        } else {
            $id = $request->post('id');
            $name = $request->post('name');
        }

        $id = filter_var($id, FILTER_VALIDATE_INT, [
            'options' => [
                'min_rage' => 0,
                'default' => 0,
            ]
        ]);

        if (!preg_match('/^[\w\d ]+$/', $name)) {
            $name = '-';
        }

        return json_encode(compact('server_ip', 'remote_ip', 'method', 'id', 'name'));
    }
}
