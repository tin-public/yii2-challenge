<?php

namespace common\log;

use yii\log\FileTarget;


class FileLogger extends FileTarget
{
    use MessagePrefixTrait;
}
