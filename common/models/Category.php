<?php
namespace common\models;

use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @return Image[]
     */
    public function getImages()
    {
        return $this->hasMany(Image::class, ['category_id' => 'category_id']);
    }
}
