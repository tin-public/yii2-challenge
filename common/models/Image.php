<?php
namespace common\models;

use yii\caching\DbDependency;
use yii\db\ActiveRecord;

class Image extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

    public static function findByParams(array $options): array
    {
        [
            'category_id' => $category_id,
            'sort_new' => $sort_new,
            'offset' => $offset,
            'limit' => $limit,
            'cache' => $cache,
        ] = $options;
        
        $query = self::find();
        
        if ($category_id) {
            $query->where(['category_id' => $category_id]);
        }

        if ($sort_new) {
            $query->orderBy(['updated_at' => SORT_DESC]);
        }

        $query->limit($limit);

        if ($offset > 1) {
            $query->offset($offset);
        }

        // Cache result in 1h
        if ($cache) {
            $query->cache(3600, new DbDependency([
                'sql' => 'SELECT max(updated_at) FROM'  . Image::tableName(),
            ]));
        }

        return $query->all();
    }

    /**
     * @return Image[]
     */
    public static function findCaptionEnd1(): array
    {
        return self::find()->where(['like', 'caption', '%1', false])->all();
    }

    public static function countByCaptionEnd1(): int
    {
        return self::find()->where(['like', 'caption', '%1', false])->count();
    }

    /**
     * @return Image[]
     */
    public static function findCaptionEnd1UsingEagerLoad(): array
    {
        return self::find()->where(['like', 'caption', '%1', false])->with('category')->all();
    }

    public static function countByParams(array $options): int
    {
        [
            'category_id' => $category_id,
        ] = $options;
        
        $query = self::find();
        
        if ($category_id) {
            $query->where(['category_id' => $category_id]);
        }

        return $query->count();
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['category_id' => 'category_id']);
    }
}
