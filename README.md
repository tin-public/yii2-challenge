# yii2-challenge

## Requirement
- macos
- docker

## How to run
```sh
cd .devcontainer
docker-compose up -d
docker-compose exec php init --env=Development --overwrite=All
docker-compose exec php composer install
docker-compose exec php yii migrate/up --interactive=0
docker-compose exec php yii migrate --migrationPath=@yii/log/migrations/ --interactive=0
```
=> The web should work at http://localhost

## Exercises
### 1. Entry script
- Open http://localhost

### 2. Db cache
- Open http://localhost/image
- Open http://localhost/debug to see if cache is working (First cache is saved).
- Reload the page and recheck in the debug page (Result is loaded from cache).
- Update db (change updated_at of a row in image table) -> Reload the page -> Look the debug page. (Cache is refreshed).

### 3. Eger Road, Lazy Road
#### Lazy Road
- Open http://localhost/image/lazy-load
- Open http://localhost/debug (Sub queries will be called during twig phase)
#### Eger Road
- Open http://localhost/image/eager-load
- Open http://localhost/debug (Sub queries will be called before twig phase)

### 4. DI container
#### Commands to create logs
```sh
curl "http://localhost/di-container?id=123&name=my name"

curl -d "id=123&name=my name" -X POST "http://localhost/di-container"
```

#### Where the logs output
- frontend/runtime/logs/app_info.log
- frontend/runtime/logs/app_error.log
- `log` table in db
